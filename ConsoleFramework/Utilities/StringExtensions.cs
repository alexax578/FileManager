﻿using System.Collections.Generic;

namespace ConsoleForms.Utilities
{
    public static class StringExtensions
    {
        public static string Center(this string stringToCenter, int totalLength) =>
            stringToCenter.PadLeft(((totalLength - stringToCenter.Length) / 2)
            + stringToCenter.Length).PadRight(totalLength);

        public static IEnumerable<string> SplitIntoSegments(this string word, int maxLength)
        {
            while (word.Length > maxLength)
            {
                yield return word.Substring(0, maxLength);
                word = word.Remove(0, maxLength);
            }

            yield return word;
        }
    }
}
