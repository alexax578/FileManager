﻿using System;
using System.Collections.Generic;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;

namespace ConsoleForms.Windows
{
    public abstract class Dialog : Window
    {
        public Point Origin { get; set; }
        public Point Position { get; set; }
        protected Dimension RelativeSize { get; set; }
        protected Point RenderPosition => Origin + Position;

        public Dialog(Dimension dimension, params KeyValuePair<ConsoleKey, Func<ResultingAction>>[] pairs) : base(pairs) =>
            RelativeSize = dimension;

        protected abstract void InnerRender(Renderer renderer);

        public override void Render(Renderer renderer)
        {
            renderer.SetViewPort(Position, Width, Height);
            renderer.ResetViewPort();
            InnerRender(renderer);
            base.Render(renderer);
        }

        public override void Resize(Point origin, int width, int height)
        {
            Origin = origin;
            Position = RelativeSize.GetPosition(width, height);
            Width = RelativeSize.GetWidth(width);
            Height = RelativeSize.GetHeight(height);
            Components.Resize(Position, Width, Height);
        }
    }
}
