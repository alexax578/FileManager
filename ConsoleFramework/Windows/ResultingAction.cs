﻿namespace ConsoleForms.Windows
{
    public class ResultingAction
    {
        public static readonly ResultingAction CloseDialog = new ResultingAction(ResultingActionType.CloseDialog);
        public static readonly ResultingAction CloseScreen = new ResultingAction(ResultingActionType.CloseScreen);
        public static readonly ResultingAction CloseDialogAndScreen = new ResultingAction(ResultingActionType.CloseDialog | ResultingActionType.CloseScreen);
        public static readonly ResultingAction Handled = new ResultingAction(ResultingActionType.Handled);
        public static readonly ResultingAction Unhandled = new ResultingAction(ResultingActionType.Unhandled);
        public static readonly ResultingAction Unfocused = new ResultingAction(ResultingActionType.Unfocused);

        public ResultingActionType Type { get; }
        public object Object { get; set; }

        public ResultingAction(Screen screen)
        {
            Type = ResultingActionType.ChangeScreen;
            Object = screen;
        }

        public ResultingAction(Dialog dialog)
        {
            Type = ResultingActionType.SpawnDialog;
            Object = dialog;
        }       

        private ResultingAction(ResultingActionType type) => Type = type;
    }
}
