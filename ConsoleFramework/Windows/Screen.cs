﻿using System;
using System.Collections.Generic;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;

namespace ConsoleForms.Windows
{
    public abstract class Screen : Window
    {
        public Screen(params KeyValuePair<ConsoleKey, Func<ResultingAction>>[] pairs) : base(pairs) {}

        public override void Render(Renderer renderer)
        {
            renderer.SetViewPort(Point.Zero, Width, Height);
            base.Render(renderer);
        }

        public override void Resize(Point origin, int width, int height)
        {
            Width = width;
            Height = height;
            Components.Resize(origin, Width, Height);
        }
    }
}
