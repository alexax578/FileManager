﻿using System;

namespace ConsoleForms.Windows
{
    [Flags]
    public enum ResultingActionType
    {
        Unhandled = 1,
        Handled = 2,
        SpawnDialog = 4,
        CloseDialog = 8,
        ChangeScreen = 16,
        CloseScreen = 32,
        Unfocused = 64
    }
}
