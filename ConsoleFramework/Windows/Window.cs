﻿using System;
using System.Collections.Generic;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;

namespace ConsoleForms.Windows
{
    public abstract class Window
    {
        public int Width { get; set; }
        public int Height { get; set; }
        protected Dictionary<ConsoleKey, Func<ResultingAction>> KeyHandles { get; set; }
        protected ComponentGroup Components { get; set; }

        public Window(params KeyValuePair<ConsoleKey, Func<ResultingAction>>[] pairs)
        {
            Components = new ComponentGroup(new Dimension(Dimension.Absolute(0), Dimension.Absolute(0), Dimension.Full, Dimension.Full));
            KeyHandles = new Dictionary<ConsoleKey, Func<ResultingAction>>();

            Components.Focused = true;

            foreach (KeyValuePair<ConsoleKey, Func<ResultingAction>> pair in pairs)
                KeyHandles.Add(pair.Key, pair.Value);
        }

        public void Open() => Components.SelectSelectable();

        public virtual ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            ResultingAction action = Components.HandleKey(keyInfo);

            if (action.Type.HasFlag(ResultingActionType.Unfocused))
                Components.Focused = true;
            else if (action.Type.HasFlag(ResultingActionType.Unhandled))
                if (KeyHandles.TryGetValue(keyInfo.Key, out Func<ResultingAction> keyHandle))
                    return keyHandle.Invoke();

            return action;
        }

        public virtual void Render(Renderer renderer)
        {           
            foreach (Component component in Components)
                component.Render(renderer, true);
        }

        public abstract void Resize(Point origin, int width, int height);
    }
}
