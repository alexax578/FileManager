﻿using Color = System.Drawing.Color;

namespace ConsoleForms.Rendering
{
    public struct Word
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Text { get; private set; }
        public Color BackgroundColor { get; set; }
        public Color ForegroundColor { get; set; }

        public Word(int x, int y, string text, Color bc, Color fc)
        {
            X = x;
            Y = y;
            Text = text;
            BackgroundColor = bc;
            ForegroundColor = fc;
        }

        public bool ColorsEqual(Color bc, Color fc) =>
            BackgroundColor == bc && ForegroundColor == fc;

        public void Append(char character) =>
            Text += character;

        public void Append(string text) =>
            Text += text;
    }
}
