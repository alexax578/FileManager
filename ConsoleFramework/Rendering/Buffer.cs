﻿using System.Collections.Generic;
using Console = Colorful.Console;
using Color = System.Drawing.Color;
using System;

namespace ConsoleForms.Rendering
{
    public sealed class Buffer
    {
        public int Width { get; set; }
        public int Height { get; set; }
        private Character[,] CurrentScreen { get; set; }
        private Character[,] NextScreen { get; set; }

        public Buffer(int width, int height)
        {
            Width = width;
            Height = height;
            CurrentScreen = new Character[Width, Height];
            NextScreen = new Character[Width, Height];
        }

        public void AddCharacter(int x, int y, char? character = null, Color? bc = null, Color? fc = null)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                if (character != null) NextScreen[x, y].Value = character.Value;
                if (bc != null) NextScreen[x, y].BackgroundColor = bc.Value;
                if (fc != null) NextScreen[x, y].ForegroundColor = fc.Value;
            }
        }

        public void Swap()
        {
            foreach (Word word in GetWords())
                PrintWord(word);

            Array.Copy(NextScreen, CurrentScreen, Width * Height);
            NextScreen = new Character[Width, Height];
        }

        public void Resize(int width, int height)
        {
            Width = width;
            Height = height;

            CurrentScreen = new Character[Width, Height];
            NextScreen = new Character[Width, Height];

            if (Width > 0 && Height > 0) Console.Clear();
        }

        private IEnumerable<Word> GetWords()
        {
            for (int y = 0; y < Height; y++)
            {
                Word word = new Word();

                for (int x = 0; x < Width; x++)
                {
                    if (CurrentScreen[x, y] != NextScreen[x, y])
                    {
                        Character character = NextScreen[x, y];

                        if (word.Text == null)
                            word = new Word(x, y, character.Value.ToString(), character.BackgroundColor, character.ForegroundColor);
                        else if ((word.ForegroundColor == character.ForegroundColor || character.Value == ' ' || character.Value == '\0') && word.BackgroundColor == character.BackgroundColor)                         
                            word.Append(character.Value);
                        else
                        {
                            yield return word;
                            word = new Word(x, y, character.Value.ToString(), character.BackgroundColor, character.ForegroundColor);
                        }
                    }
                    else if (word.Text != null)
                    {
                        yield return word;
                        word = new Word();
                    }
                }

                if (word.Text != null)
                    yield return word;
            }
        }

        private static void PrintWord(Word word)
        {
            try { Console.SetCursorPosition(word.X, word.Y); } catch { return; };
            Console.BackgroundColor = word.BackgroundColor;
            Console.ForegroundColor = word.ForegroundColor;
            Console.Write(word.Text.Replace("\0", " "));
        }
    }
}
