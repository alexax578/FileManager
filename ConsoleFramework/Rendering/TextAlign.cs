﻿namespace ConsoleForms.Rendering
{
    public enum TextAlign
    {
        Left,
        Center,
        Right
    }
}
