﻿using ConsoleForms.Data_Structures;

namespace ConsoleForms.Rendering
{
    public struct ViewPort
    {
        private Point Position { get; set; }
        private int Width { get; set; }
        private int Height { get; set; }

        public ViewPort(Point position, int width, int height)
        {
            Position = position;
            Width = width;
            Height = height;
        }

        public bool IsInside(int x, int y) =>
            x >= Position.X && x < Position.X + Width &&
            y >= Position.Y && y < Position.Y + Height;
    }
}
