﻿namespace ConsoleForms.Rendering
{
    public struct BoxCharacter
    {
        public const char TopLeft = '╔';
        public const char TopRight = '╗';
        public const char BottomLeft = '╚';
        public const char BottomRight = '╝';
        public const char Horizontal = '═';
        public const char Vertical = '║';
        public const char VerticalSeparator = '│';
    }
}
