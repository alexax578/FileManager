﻿using System;
using Color = System.Drawing.Color;

namespace ConsoleForms.Rendering
{
    public struct Character : IEquatable<Character>
    {
        public char Value { get; set; }
        public Color BackgroundColor { get; set; }
        public Color ForegroundColor { get; set; }

        public Character(char value, Color bc, Color fc)
        {
            Value = value;
            BackgroundColor = bc;
            ForegroundColor = fc;
        }

        public bool ColorsEqual(Color bc, Color fc) =>
            BackgroundColor == bc && ForegroundColor == fc;

        public static bool operator ==(Character c1, Character c2) =>
            c1.Value == c2.Value &&
            c1.BackgroundColor == c2.BackgroundColor &&
            c1.ForegroundColor == c2.ForegroundColor;

        public static bool operator !=(Character c1, Character c2) =>
            c1.Value != c2.Value ||
            c1.BackgroundColor != c2.BackgroundColor ||
            c1.ForegroundColor != c2.ForegroundColor;

        public override bool Equals(object obj) =>
            obj is Character character && Equals(character);

        public bool Equals(Character other) =>
            Value == other.Value &&
            BackgroundColor.Equals(other.BackgroundColor) &&
            ForegroundColor.Equals(other.ForegroundColor);

        public override int GetHashCode()
        {
            int hashCode = -904441327;
            hashCode *= -1521134295 + base.GetHashCode();
            hashCode *= -1521134295 + Value.GetHashCode();
            hashCode *= -1521134295 + BackgroundColor.GetHashCode();
            hashCode *= -1521134295 + ForegroundColor.GetHashCode();
            return hashCode;
        }
    }
}
