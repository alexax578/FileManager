﻿using ConsoleForms.Data_Structures;
using ConsoleForms.Utilities;
using Console = Colorful.Console;
using Color = System.Drawing.Color;

namespace ConsoleForms.Rendering
{
    public class Renderer
    {
        private Buffer Buffer { get; set; }

        private ViewPort ViewPort { get; set; }
        public int Width => Buffer.Width;
        public int Height => Buffer.Height;

        public Renderer(int width, int height)
        {
            Buffer = new Buffer(width, height);
            ResetViewPort();

            /* Change the screen color to default */
            Console.BackgroundColor = Color.Black;
            Console.Clear();
        }

        public void AddLine(int x, int y, int size, char? character = null, Color? bc = null, Color? fc = null, Direction direction = Direction.Right)
        {
            if (direction == Direction.Right)
                for (int i = x; i < x + size; i++)
                    AddCharacter(i, y, character, bc, fc);

            else if (direction == Direction.Down)
                for (int i = y; i < y + size; i++)
                    AddCharacter(x, i, character, bc, fc);
        }

        public void AddBox(int x, int y, int width, int height, Color? bc = null, Color? fc = null)
        {
            //Corners
            AddLine(x, y, 1, BoxCharacter.TopLeft, bc, fc);
            AddLine(x + width - 1, y, 1, BoxCharacter.TopRight, bc, fc);
            AddLine(x, y + height - 1, 1, BoxCharacter.BottomLeft, bc, fc);
            AddLine(x + width - 1, y + height - 1, 1, BoxCharacter.BottomRight, bc, fc);

            //Lines
            AddLine(x + 1, y, width - 2, BoxCharacter.Horizontal, bc, fc);
            AddLine(x + 1, y + height - 1, width - 2, BoxCharacter.Horizontal, bc, fc);
            AddLine(x, y + 1, height - 2, BoxCharacter.Vertical, bc, fc, Direction.Down);
            AddLine(x + width - 1, y + 1, height - 2, BoxCharacter.Vertical, bc, fc, Direction.Down);
        }

        public void AddBoxWithSeparators(int x, int y, int width, int height, int[] positions, Color? bc = null, Color? fc = null)
        {
            AddBox(x, y, width, height, bc, fc);

            int pos = 0;
            foreach (int position in positions)
            {
                pos += position;
                AddLine(x + pos, y + 1, height - 2, BoxCharacter.VerticalSeparator, bc, fc, Direction.Down);
            }
        }

        public void FillBox(int x, int y, int width, int height, char? character = null, Color? bc = null, Color? fc = null)
        {
            for (int i = 0; i < height; i++)
                AddLine(x, y + i, width, character, bc, fc);
        }

        public void AddText(int x, int y, int width, string text, Color? bc = null, Color? fc = null, TextAlign textAlign = TextAlign.Left, bool fillToWidth = false)
        {
            switch (textAlign)
            {
                case TextAlign.Left:
                    if (fillToWidth)
                        text = text.PadRight(width);

                    for (int i = 0; i < text.Length && i < width; i++)
                        AddCharacter(x + i, y, text[i], bc, fc);
                    break;

                case TextAlign.Center:
                    if (fillToWidth)
                        text = text.Center(width);

                    for (int i = 0; i < text.Length && i < width; i++)
                        AddCharacter(x + width / 2 - text.Length / 2 + i, y, text[i], bc, fc);
                    break;

                case TextAlign.Right:
                    if (fillToWidth)
                        text = text.PadLeft(width);

                    for (int i = 0; i < text.Length && i < width; i++)
                        AddCharacter(x + width - (text.Length - i), y, text[i], bc, fc);
                    break;
            }
        }

        public void SetViewPort(Point position, int width, int height) =>
            ViewPort = new ViewPort(position, width, height);

        public void ResetViewPort() => ViewPort = new ViewPort(Point.Zero, Width, Height);

        public void Render() => Buffer.Swap();

        public void Resize(int width, int height) =>
            Buffer.Resize(width, height);

        private void AddCharacter(int x, int y, char? character = null, Color? bc = null, Color? fc = null)
        {
            if (ViewPort.IsInside(x, y)) // ( ͡° ͜ʖ ͡°)  
                Buffer.AddCharacter(x, y, character, bc, fc);
        }
    }
}
