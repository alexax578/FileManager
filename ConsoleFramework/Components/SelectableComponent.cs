﻿using System;
using ConsoleForms.Data_Structures;
using ConsoleForms.Windows;

namespace ConsoleForms.Components
{
    public abstract class SelectableComponent : Component
    {
        protected bool selected;
        public bool Selected { get => selected;
            set
            {
                selected = value;
                if (selected) OnSelection?.Invoke();
                else OnDeselection?.Invoke();
            }
        }
        public event Action OnSelection;
        public event Action OnDeselection;

        public SelectableComponent(Dimension dimension) 
            : base(dimension) {}

        public abstract ResultingAction HandleKey(ConsoleKeyInfo keyInfo);
    }
}
