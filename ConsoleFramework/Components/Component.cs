﻿ using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;

namespace ConsoleForms.Components
{
    public abstract class Component
    {
        public Point Origin { get; set; }
        public Point Position { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        protected Dimension RelativeSize { get; set; }
        public Point RenderPosition => Origin + Position;

        public Component(Dimension relativeSize) =>
            RelativeSize = relativeSize;

        public abstract void Render(Renderer renderer, bool focused);

        public virtual void Resize(Point origin, int width, int height)
        {
            Origin = origin;
            Position = RelativeSize.GetPosition(width, height);
            Width = RelativeSize.GetWidth(width);
            Height = RelativeSize.GetHeight(height);
        }
    }
}
