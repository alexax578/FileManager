﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace ConsoleForms.Components
{
    public class ComponentGroup : SelectableComponent, IEnumerable<Component>
    {
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        protected List<Component> Components { get; set; }
        protected SelectableComponent selectedComponent;
        public SelectableComponent SelectedComponent
        {
            get => selectedComponent;
            set
            {
                if (selectedComponent != null) selectedComponent.Selected = false;
                selectedComponent = value;
                if (selectedComponent != null) selectedComponent.Selected = true;
            }
        }
        public bool Focused { get; set; }

        public ComponentGroup(Dimension dimension) 
            : base(dimension) =>
            Components = new List<Component>();

        public override void Render(Renderer renderer, bool focused)
        {
            foreach (Component component in Components)
                component.Render(renderer, Selected);
        }

        public void Add(Component component)
        {
            if (SelectedComponent == null && component is SelectableComponent selectable)
                SelectedComponent = selectable;

            Components.Add(component);
        }

        public IEnumerator<Component> GetEnumerator() => Components.GetEnumerator();

        public void SelectSelectable()
        {
            List<SelectableComponent> components = GetSelectable().ToList();

            if (components.Count() > 0)
                SelectedComponent = components[0];
        }

        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            ResultingAction action = ResultingAction.Unhandled;

            if (selectedComponent != null)
                action = selectedComponent.HandleKey(keyInfo);

            if (action.Type.HasFlag(ResultingActionType.Unhandled) && keyInfo.Key == ConsoleKey.Tab && Focused)
            {
                if (keyInfo.Modifiers == ConsoleModifiers.Shift && SelectedComponent is ComponentGroup group)
                {
                    if (group.GetSelectable().Count() > 1)
                    {
                        Focused = false;
                        group.Focused = true;
                        group.SelectNext();
                        return ResultingAction.Handled;
                    }
                }

                if (keyInfo.Modifiers == ConsoleModifiers.Control)
                {
                    Focused = false;
                    return ResultingAction.Unfocused;
                }

                if (keyInfo.Modifiers == 0)
                {
                    SelectNext();
                    return ResultingAction.Handled;
                }

                return ResultingAction.Unhandled;
            }
            else if (action.Type.HasFlag(ResultingActionType.Unfocused))
            {
                Focused = true;
                SelectNext();
                return ResultingAction.Handled;
            }
        
            return action;
        }

        public override void Resize(Point origin, int width, int height)
        {
            base.Resize(origin, width, height);

            foreach (Component component in Components)
                component.Resize(RenderPosition, Width, Height);
        }

        private void SelectNext()
        {
            List<SelectableComponent> selectable = GetSelectable().ToList();
            int index = selectable.IndexOf(SelectedComponent) + 1;

            if (selectable.Count > 0)
                SelectedComponent = selectable[index % selectable.Count];
        }

        private IEnumerable<SelectableComponent> GetSelectable()
        {
            foreach (Component component in Components)
            {
                if (component is ComponentGroup group)
                {
                    if (group.GetSelectable().Count() > 0) yield return group;
                }
                else if (component is SelectableComponent active) yield return active;
            }
        }
    }
}
