﻿using System;
using System.Collections.Generic;
using System.Threading;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace ConsoleForms
{
    public abstract class Application
    {
        private const int ON_RESIZE_SLEEP_INTERVAL = 150;

        protected Renderer Renderer { get; set; }
        protected Stack<Screen> Screens { get; set; }
        protected List<Dialog> Dialogs { get; set; }

        public Application(int width, int height)
        {
            Renderer = new Renderer(width, height);
            Screens = new Stack<Screen>();
            Dialogs = new List<Dialog>();          
        }

        public void Run()
        {
            Console.SetWindowSize(Renderer.Width, Renderer.Height);
            Resize(Renderer.Width, Renderer.Height);
            Render();

            while (true)
            {
                if (Console.KeyAvailable)
                {
                    HandleKey(Console.ReadKey(true));
                    Render();
                }

                if (CheckResize())
                    Render();
            }
        }

        private void AddScreen(Screen screen)
        {
            Screens.Push(screen);
            screen.Open();
            Resize(Renderer.Width, Renderer.Height);
        }

        private void RemoveScreen()
        {
            Screens.Pop();
            Resize(Renderer.Width, Renderer.Height);
        }

        private void AddDialog(Dialog dialog)
        {
            Dialogs.Add(dialog);
            dialog.Open();
            Resize(Renderer.Width, Renderer.Height);
        }

        private void HandleKey(ConsoleKeyInfo keyInfo)
        {
            ResultingAction result = (Dialogs.Count > 0)
                ? Dialogs[Dialogs.Count - 1].HandleKey(keyInfo)
                : Screens.Peek().HandleKey(keyInfo);

            if (result.Type.HasFlag(ResultingActionType.ChangeScreen))
                AddScreen(result.Object as Screen);

            if (result.Type.HasFlag(ResultingActionType.CloseScreen))
                RemoveScreen();

            if (result.Type.HasFlag(ResultingActionType.SpawnDialog))
                AddDialog(result.Object as Dialog);
                
            if (result.Type.HasFlag(ResultingActionType.CloseDialog))
                Dialogs.RemoveAt(Dialogs.Count - 1);
        }

        private void Render()
        {
            Screens.Peek().Render(Renderer);
        
            foreach (Dialog dialog in Dialogs)
                dialog.Render(Renderer);

            Renderer.Render();
        }

        private void Resize(int width, int height)
        {
            Screens.Peek().Resize(Point.Zero, Renderer.Width, Renderer.Height);

            foreach (Dialog dialog in Dialogs)
                dialog.Resize(Point.Zero, Renderer.Width, Renderer.Height);

            Console.CursorVisible = false;
        }

        private void ResizeBuffer(int width, int height)
        {
            Renderer.Resize(width, height);
            Resize(width, height);
        }

        private bool CheckResize()
        {         
            if (Console.WindowWidth != Renderer.Width || Console.WindowHeight != Renderer.Height)
            {
                int width, height;

                do
                {
                    width = Console.WindowWidth;
                    height = Console.WindowHeight;
                    Thread.Sleep(ON_RESIZE_SLEEP_INTERVAL);
                } while (width != Console.WindowWidth || height != Console.WindowHeight);

                ResizeBuffer(width, height);
                return true;
            }

            return false;
        }
    }

    //Matyáš magic.
    public class Application<T> : Application where T : Screen, new()
    {
        public Application(int width, int height) : base(width, height) =>
            Screens.Push(new T());     
    }
}
