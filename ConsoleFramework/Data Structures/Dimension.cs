﻿namespace ConsoleForms.Data_Structures
{
    public delegate int Size(int size);

    public struct Dimension
    {
        public static readonly Size Full = (size) => size;
        public static readonly Size Half = (size) => size / 2;
        public static readonly Size Quarter = (size) => size / 4;
        public static readonly Size Eighth = (size) => size / 8;
        public static readonly Size Tenth = (size) => size / 10;

        public static Size Absolute(int size) => (s) => size;
        public static Size Offset(int offset) => (size) => size - offset;

        private Size RelativeX { get; }
        private Size RelativeY { get; }
        private Size RelativeWidth { get; }
        private Size RelativeHeight { get; }

        public Dimension(Size positionX, Size positionY, Size width, Size height)
        {
            RelativeX = positionX;
            RelativeY = positionY;
            RelativeWidth = width;
            RelativeHeight = height;
        }

        public Point GetPosition(int width, int height) =>
            new Point(RelativeX.Invoke(width), RelativeY.Invoke(height));

        public int GetWidth(int width) =>
            RelativeWidth.Invoke(width);

        public int GetHeight(int height) =>
            RelativeHeight.Invoke(height);
    }
}
