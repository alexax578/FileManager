﻿namespace ConsoleForms.Data_Structures
{
    public struct Point
    {
        public static readonly Point Zero = new Point(0, 0);

        public int X { get; set;  }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point operator +(Point pointA, Point pointB) =>
            new Point(pointA.X + pointB.X, pointA.Y + pointB.Y);
    }
}
