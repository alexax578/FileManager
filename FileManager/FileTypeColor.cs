﻿using Color = System.Drawing.Color;

namespace FileManager
{
    struct FileTypeColor
    {
        public static readonly Color Directory = Color.White;
        public static readonly Color File = Color.FromArgb(170, 170, 170);
    }
}
