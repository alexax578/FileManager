﻿using FileManager.Components;
using FileManager.Data_Structures;
using FileManager.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using ConsoleForms.Data_Structures;
using ConsoleForms.Windows;
using ConsoleForms.Components;

namespace FileManager.Screens
{
    class ExplorerScreen : Screen
    {
        public ExplorerScreen() : base(
            new KeyValuePair<ConsoleKey, Func<ResultingAction>>(ConsoleKey.D1, () => new ResultingAction(new HelpDialog(new Dimension(size => size / 2 - 20, size => size / 2 - 3, Dimension.Absolute(40), Dimension.Absolute(6))))))
        {
            string root = Path.GetPathRoot(Environment.SystemDirectory);
            Components.Add(new FileExplorer(new Dimension(Dimension.Absolute(0), Dimension.Absolute(0), Dimension.Half, Dimension.Offset(1)), root, RefreshExplorers));
            Components.Add(new FileExplorer(new Dimension(Dimension.Half, Dimension.Absolute(0), Dimension.Half, Dimension.Offset(1)), root, RefreshExplorers));
            Components.Add(new InfoBar(new Dimension(Dimension.Absolute(0), Dimension.Offset(1), Dimension.Full, Dimension.Absolute(1)),
                new KeyLabelPair(ConsoleKey.D1, "help"),
                new KeyLabelPair(ConsoleKey.D2, "mkdir"),
                new KeyLabelPair(ConsoleKey.D3, "del"),
                new KeyLabelPair(ConsoleKey.D4, "ren"),
                new KeyLabelPair(ConsoleKey.D5, "move"),
                new KeyLabelPair(ConsoleKey.D6, "copy"),
                new KeyLabelPair(ConsoleKey.D7, "paste"),
                new KeyLabelPair(ConsoleKey.D8, "touch"),
                new KeyLabelPair(ConsoleKey.F5, "refresh")));
        }

        public void RefreshExplorers()
        {
            foreach (Component component in Components)
                if (component is FileExplorer explorer)
                    explorer.Refresh();
        }
    }
}
