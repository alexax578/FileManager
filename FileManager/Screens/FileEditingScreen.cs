﻿using System;
using System.Collections.Generic;
using System.IO;
using ConsoleForms.Data_Structures;
using ConsoleForms.Windows;
using FileManager.Components;
using FileManager.Dialogs;

namespace FileManager.Screens
{
    class FileEditingScreen : Screen
    {
        public FileEditingScreen(FileInfo file, params KeyValuePair<ConsoleKey, Func<ResultingAction>>[] pairs)
            : base(pairs)
        {
            FileEditor editor = new FileEditor(new Dimension(Dimension.Absolute(0), Dimension.Absolute(0), Dimension.Full, Dimension.Full), file);
            Components.Add(editor);
            KeyHandles.Add(ConsoleKey.Escape, () => new ResultingAction(new FileEditingCloseDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), editor, file)));
        }
    }
}
