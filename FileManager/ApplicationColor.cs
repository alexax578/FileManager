﻿using Color = System.Drawing.Color;

namespace FileManager
{
    struct ApplicationColor
    {
        public static readonly Color Background = Color.Black;
        public static readonly Color LighterBackground = Color.FromArgb(30, 30, 30);
        public static readonly Color Component = Color.FromArgb(0, 0, 170);
        public static readonly Color Dialog = Color.FromArgb(100, 100, 100);
        public static readonly Color Error = Color.FromArgb(170, 0, 0);
        public static readonly Color Foreground = Color.White;
        public static readonly Color ScrollTableHeader = Color.FromArgb(255, 215, 0);
        public static readonly Color InfoBar = Color.FromArgb(0, 170, 170);
        public static readonly Color SelectedBackground = Color.FromArgb(130, 140, 0);
        public static readonly Color SelectedForeground = Color.Black;
    }
}
