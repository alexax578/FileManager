﻿using FileManager.Data_Structures;
using System.Collections.Generic;
using System.IO;

namespace FileManager.Utilities
{
    public static class RowService
    {
        public static IEnumerable<string> GetDriveNames(IEnumerable<DriveInfo> drives)
        {
            foreach (DriveInfo drive in drives)
                yield return drive.Name;
        }

        public static IEnumerable<ScrollTableRow> GetDriveRows(IEnumerable<DriveInfo> drives)
        {
            foreach (DriveInfo drive in drives)
                yield return new ScrollTableRow(FileTypeColor.Directory, drive.Name);
        }

        public static IEnumerable<string> GetFileSystemInfoNames(IEnumerable<FileSystemInfo> infos)
        {
            foreach (FileSystemInfo info in infos)
                yield return info.Name;
        }

        public static IEnumerable<ScrollTableRow> GetFileSystemInfoRows(IEnumerable<FileSystemInfo> infos)
        {
            foreach (FileSystemInfo info in infos)
            {
                if (info is FileInfo file)
                    yield return new ScrollTableRow(FileTypeColor.File, file.Name, FileSizeFormatter.Format(file.Length, FileSize.B), file.LastAccessTime.ToString());
                else if (info is DirectoryInfo directory)
                    yield return new ScrollTableRow(FileTypeColor.Directory, directory.Name, "", directory.LastAccessTime.ToString());
            }
        }
    }
}
