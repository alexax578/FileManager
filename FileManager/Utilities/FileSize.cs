﻿namespace FileManager.Utilities
{
    public enum FileSize
    {
        B = 1,
        KB = 2,
        MB = 3,
        GB = 4,
        PB = 5,
        EB = 6       
    }
}
