﻿using System;

namespace FileManager.Utilities
{
    public static class FileSizeFormatter
    {
        public static string Format(decimal value, FileSize size)
        {
            while (value > 1024)
            {
                value /= 1024;
                size++;
            }

            return $"{decimal.Round(value, 2, MidpointRounding.AwayFromZero)} {size.ToString()}";
        }
    }
}
