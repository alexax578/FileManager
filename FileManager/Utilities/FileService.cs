﻿using System.Collections.Generic;
using System.IO;

namespace FileManager.Utilities
{
    public static class FileService
    {
        public static void CopyDirectory(DirectoryInfo directory, string location)
        {
            string path = Path.Combine(location, directory.Name);

            Directory.CreateDirectory(path);

            foreach (DirectoryInfo dir in directory.GetDirectories("*", SearchOption.AllDirectories))
                Directory.CreateDirectory(Path.Combine(path, dir.FullName.Remove(0, directory.FullName.Length + 1)));

            foreach (FileInfo file in directory.GetFiles("*", SearchOption.AllDirectories))
                file.CopyTo(Path.Combine(path, file.FullName.Remove(0, directory.FullName.Length + 1)));
        }

        public static IEnumerable<string> GetRowsFromFile(FileInfo info)
        {
            using (StreamReader streamReader = new StreamReader(info.FullName))
            {
                while (!streamReader.EndOfStream)
                    yield return streamReader.ReadLine();
            }
        }

        public static void RewriteFile(FileInfo file, List<string> text)
        {
            using (StreamWriter writer = new StreamWriter(file.FullName))
                foreach (string line in text)
                    writer.WriteLine(line);
        }
    }
}
