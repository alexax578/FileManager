﻿using System.Collections.Generic;
using System.Linq;

namespace FileManager.Utilities
{
    public static class LINQExtensions
    {
        public static IEnumerable<T> DropLast<T>(this IEnumerable<T> enumerable) =>
            enumerable.Take(enumerable.Count() - 1);
    }
}
