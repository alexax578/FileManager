﻿using System;

namespace FileManager.Data_Structures
{
    public struct KeyLabelPair
    {
        public ConsoleKey Key { get; set; }
        public string Value { get; set; }

        public KeyLabelPair(ConsoleKey key, string value)
        {
            Key = key;
            Value = value;
        }       
    }
}
