﻿using ConsoleForms.Rendering;
using Color = System.Drawing.Color;

namespace FileManager.Data_Structures
{
    public struct ScrollTableRow
    {
        public string[] Values { get; set; }
        public Color TextColor { get; set; }

        public ScrollTableRow(Color textColor, params string[] values)
        {
            TextColor = textColor;
            Values = values;
        }
    }
}
