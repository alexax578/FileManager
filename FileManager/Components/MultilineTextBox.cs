﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Components
{
    //TODO: Finish
    class MultilineTextBox : SelectableComponent
    {
        private Point cursorPosition;

        private int CursorMaximumX { get; set; }
        private Point RenderingStart { get; set; }
        private Point CursorPosition
        {
            get => cursorPosition;
            set
            {
                cursorPosition = value;
                OnCursorMove.Invoke(CursorPosition);
            }
        }
        public List<string> Rows { get; private set; }

        public event Action<Point> OnCursorMove;

        public MultilineTextBox(Dimension dimension, List<string> rows = null) : base(dimension)
        {
            Rows = rows ?? new List<string>();
            if (Rows.Count == 0) Rows.Add("");
        }

        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            switch (keyInfo.Key)
            {
                case ConsoleKey.LeftArrow:
                    MoveLeft();
                    return ResultingAction.Handled;

                case ConsoleKey.RightArrow:
                    MoveRight();
                    return ResultingAction.Handled;

                case ConsoleKey.UpArrow:
                    MoveUp();
                    return ResultingAction.Handled;

                case ConsoleKey.DownArrow:
                    MoveDown();
                    return ResultingAction.Handled;

                case ConsoleKey.Enter:
                    AddLine();
                    return ResultingAction.Handled;

                case ConsoleKey.Backspace:
                    DeleteCharacter();
                    return ResultingAction.Handled;
            }

            if (!char.IsControl(keyInfo.KeyChar) || keyInfo.Key == ConsoleKey.Tab)
            {
                WriteCharacter(keyInfo.KeyChar);
                return ResultingAction.Handled;
            }

            return ResultingAction.Unhandled;
        }

        public override void Render(Renderer renderer, bool focused)
        {
            List<string> rows = Rows.Select(i => i.Replace("\t", " ")).ToList(); //Remove tabs before rendering.

            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, bc: ApplicationColor.Background);

            for (int i = 0; i < rows.Count - RenderingStart.Y && i < Height; i++)
            {
                int substring = rows[i + RenderingStart.Y].Length > RenderingStart.X ? RenderingStart.X : rows[i + RenderingStart.Y].Length;
                renderer.AddText(RenderPosition.X, RenderPosition.Y + i, Width, rows[i + RenderingStart.Y].Substring(substring, rows[i + RenderingStart.Y].Length - substring), ApplicationColor.Background, ApplicationColor.Foreground, fillToWidth: true);
            }

            renderer.AddLine(RenderPosition.X + CursorPosition.X - RenderingStart.X, RenderPosition.Y + CursorPosition.Y - RenderingStart.Y, 1, bc: ApplicationColor.SelectedBackground);
        }

        public override void Resize(Point origin, int width, int height)
        {
            int prevWidth = Width;
            int prevHeight = Height;
            base.Resize(origin, width, height);

            if (prevWidth != 0 && prevHeight != 0 && Width != 0 && Height != 0)
                RecalculatePositions(prevWidth, prevHeight);
        }

        public void ChangeRows(List<string> rows) =>
            Rows = rows;

        private void WriteCharacter(char character)
        {
            Rows[CursorPosition.Y] = Rows[CursorPosition.Y].Insert(CursorPosition.X, character.ToString());
            MoveRight();
        }

        private void DeleteCharacter()
        {
            string row = Rows[CursorPosition.Y];

            if (row.Length > 0 && CursorPosition.X > 0)
            {
                Rows[CursorPosition.Y] = row.Remove(CursorPosition.X - 1, 1);
                MoveLeft();
            }
            else if (CursorPosition.Y > 0)
            {
                CursorPosition = new Point(CursorPosition.X, CursorPosition.Y - 1);
                CursorMaximumX = Rows[CursorPosition.Y].Length;
                Update();
                Rows[CursorPosition.Y] += Rows[CursorPosition.Y + 1];
                Rows.RemoveAt(CursorPosition.Y + 1);
            }
        }

        private void AddLine()
        {
            string row = Rows[CursorPosition.Y].Substring(CursorPosition.X);

            if (CursorPosition.X < Rows[CursorPosition.Y].Length)
                Rows[CursorPosition.Y] = Rows[CursorPosition.Y].Remove(CursorPosition.X);

            Rows.Insert(CursorPosition.Y + 1, row);
            CursorPosition = new Point(0, CursorPosition.Y + 1);
            CursorMaximumX = 0;
            Update();
        }

        private void MoveLeft()
        {
            if (CursorPosition.X > 0)
            {
                CursorPosition = new Point(CursorPosition.X - 1, CursorPosition.Y);
                CursorMaximumX = CursorPosition.X;

                Update();
            }
        }

        private void MoveRight()
        {
            if (CursorPosition.X < Rows[CursorPosition.Y].Length)
            {
                CursorPosition = new Point(CursorPosition.X + 1, CursorPosition.Y);
                CursorMaximumX = CursorPosition.X;

                Update();
            }
        }

        private void MoveUp()
        {
            if (CursorPosition.Y > 0)
                CursorPosition = new Point(CursorPosition.X, CursorPosition.Y - 1);

            Update();
        }

        private void MoveDown()
        {
            if (CursorPosition.Y < Rows.Count - 1)
                CursorPosition = new Point(CursorPosition.X, CursorPosition.Y + 1);

            Update();
        }

        private void UpdateCursorPosition()
        {
            if (CursorMaximumX > Rows[CursorPosition.Y].Length)
                CursorPosition = new Point(Rows[CursorPosition.Y].Length, CursorPosition.Y);
            else
                CursorPosition = new Point(CursorMaximumX, CursorPosition.Y);          
        }

        private void UpdateRenderingPosition()
        {
            if (CursorPosition.Y < RenderingStart.Y)
                RenderingStart = new Point(RenderingStart.X, RenderingStart.Y - 1);

            if (CursorPosition.Y >= RenderingStart.Y + Height)
                RenderingStart = new Point(RenderingStart.X, RenderingStart.Y + 1);

            while (CursorPosition.X < RenderingStart.X)
                RenderingStart = new Point(RenderingStart.X - Width, RenderingStart.Y);

            while (CursorPosition.X >= Width + RenderingStart.X)
                RenderingStart = new Point(RenderingStart.X + Width, RenderingStart.Y);
        }

        private void Update()
        {
            UpdateCursorPosition();
            UpdateRenderingPosition();
        }

        private void RecalculatePositions(int prevWidth, int prevHeight)
        {
            int widthsAway = CursorPosition.X / Width;
            float heightPercentage = (CursorPosition.Y - RenderingStart.Y) / (float)prevHeight;

            int renderStartY = CursorPosition.Y - (int)(heightPercentage * Height);
            RenderingStart = new Point(widthsAway * Width, renderStartY >= 0 ? renderStartY : 0);
        }
    }
}
