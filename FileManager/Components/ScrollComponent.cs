﻿using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Windows;
using System;
using System.Collections.Generic;

namespace FileManager.Components
{
    abstract class ScrollComponent<T> : SelectableComponent
    {
        protected List<T> Rows { get; set; }       
        protected int RenderStartingRow { get; set; }
        protected int? MultiselectStart { get; set; }
        protected bool Multiselect { get; set; }
        public int SelectedRowIndex { get; set; }

        public virtual Point ScrollRenderPosition => RenderPosition;
        public virtual int ScrollWidth => Width;
        public virtual int ScrollHeight => Height;

        public ScrollComponent(Dimension dimension, bool multiselect = false) : base(dimension)
        {
            MultiselectStart = null;
            Multiselect = multiselect;
            Rows = new List<T>();
        }

        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            if (keyInfo.Modifiers == ConsoleModifiers.Alt && (keyInfo.Key == ConsoleKey.UpArrow || keyInfo.Key == ConsoleKey.DownArrow) && !MultiselectStart.HasValue)
                MultiselectStart = SelectedRowIndex;
            else
                MultiselectStart = null;

            if (keyInfo.Key == ConsoleKey.UpArrow)
            {             
                MoveUp();
                return ResultingAction.Handled;
            }
            else if (keyInfo.Key == ConsoleKey.DownArrow)
            {
                MoveDown();
                return ResultingAction.Handled;
            }
            else if (keyInfo.Key == ConsoleKey.Home)
            {
                SelectedRowIndex = 0;
                RenderStartingRow = 0;
            }
            else if (keyInfo.Key == ConsoleKey.End)
            {
                SelectedRowIndex = Rows.Count - 1;
                RenderStartingRow = SelectedRowIndex - ScrollHeight + 1 > 0 ? SelectedRowIndex - ScrollHeight + 1 : 0;
            }

            return ResultingAction.Unhandled;
        }

        public virtual void ChangeRows(List<T> rows)
        {
            Rows = rows;
            SelectedRowIndex = 0;
            RenderStartingRow = 0;
        }       

        protected void MoveUp()
        {
            if (SelectedRowIndex > 0)
            {
                SelectedRowIndex--;

                if (SelectedRowIndex < RenderStartingRow)
                    RenderStartingRow--;
            }
        }

        protected void MoveDown()
        {
            if (SelectedRowIndex < Rows.Count - 1)
            {
                SelectedRowIndex++;

                if (SelectedRowIndex >= RenderStartingRow + ScrollHeight)
                    RenderStartingRow++;
            }
        }

        public override void Resize(Point origin, int width, int height)
        {
            base.Resize(origin, width, height);

            //Move RenderStartingRow so that selected row is in the rendering range.
            if (RenderStartingRow + ScrollHeight <= SelectedRowIndex)
                RenderStartingRow = RenderStartingRow + (SelectedRowIndex - RenderStartingRow - ScrollHeight + 1);

            else if (RenderStartingRow >= SelectedRowIndex)
                RenderStartingRow = SelectedRowIndex - ScrollHeight + 1 > 0 ? SelectedRowIndex - ScrollHeight + 1 : 0; 
        }
    }
}
