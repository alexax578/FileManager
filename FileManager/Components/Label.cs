﻿using System;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using Color = System.Drawing.Color;

namespace FileManager.Components
{
    class Label : Component
    {
        public string Text { get; set; }
        public Color? BackgroundColor { get; set; }
        public Color? ForegroundColor { get; set; }
        public TextAlign TextAlign { get; set; }
        public bool FillToWidth { get; set; }

        public Label(Dimension dimension, string text, Color? bc = null, Color? fc = null, TextAlign textAlign = TextAlign.Left, bool fillToWidth = false) : base(dimension)
        {
            BackgroundColor = bc;
            ForegroundColor = fc;
            Text = text;
            TextAlign = textAlign;
            FillToWidth = fillToWidth;
        }

        public override void Render(Renderer renderer, bool focused) =>
            renderer.AddText(RenderPosition.X, RenderPosition.Y, Width, Text, BackgroundColor, ForegroundColor, TextAlign, FillToWidth);
    }
}
