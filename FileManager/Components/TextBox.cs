﻿using System;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;
using Color = System.Drawing.Color;

namespace FileManager.Components
{
    class TextBox : SelectableComponent
    {
        public int CursorPosition { get; set; }
        public int RenderingStart { get; set; }
        public string Text { get; set; }
        public Color? BackgroundColor { get; set; }
        public Color? ForegroundColor { get; set; }
        public TextAlign TextAlign { get; set; }
        public bool FillToWidth { get; set; }

        public TextBox(Dimension dimension, string text = "", Color? bc = null, Color? fc = null, 
            TextAlign textAlign = TextAlign.Left, bool fillToWidth = true) : base(dimension)
        {
            BackgroundColor = bc;
            ForegroundColor = fc;
            TextAlign = textAlign;
            FillToWidth = fillToWidth;
            Text = text;
        }

        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            switch (keyInfo.Key)
            {
                case ConsoleKey.LeftArrow:
                    MoveLeft();
                    return ResultingAction.Handled;

                case ConsoleKey.RightArrow:
                    MoveRight();
                    return ResultingAction.Handled;

                case ConsoleKey.Backspace:
                    DeleteCharacter();
                    return ResultingAction.Handled;
            }

            if (!char.IsControl(keyInfo.KeyChar))
            {
                WriteCharacter(keyInfo.KeyChar);
                return ResultingAction.Handled;
            }

            return ResultingAction.Unhandled;
        }

        public override void Render(Renderer renderer, bool focused)
        {
            TextAlign textAlign = (TextAlign == TextAlign.Center && (Text.Length > Width || (Selected && focused))) ? TextAlign.Left : TextAlign;
            int substring = Text.Length > RenderingStart ? RenderingStart : Text.Length;
            renderer.AddText(RenderPosition.X, RenderPosition.Y, Width, Text.Substring(substring, Text.Length - substring), BackgroundColor, ForegroundColor, textAlign, FillToWidth);
            if (Selected && focused)
            {
                switch (textAlign)
                {
                    case TextAlign.Left:
                        renderer.AddLine(RenderPosition.X + CursorPosition - RenderingStart, RenderPosition.Y, 1, bc: ApplicationColor.SelectedBackground, fc: ApplicationColor.SelectedForeground);
                        break;

                    case TextAlign.Center:
                        renderer.AddLine(RenderPosition.X + Width / 2 - Text.Length / 2 + CursorPosition, RenderPosition.Y, 1, bc: ApplicationColor.SelectedBackground, fc: ApplicationColor.SelectedForeground);
                        break;

                    case TextAlign.Right:
                        throw new NotImplementedException();
                }
            }
        }

        public override void Resize(Point origin, int width, int height)
        {
            base.Resize(origin, width, height);
            RecalculatePositions();
        }

        public void RecalculatePositions()
        {
            CursorPosition = Text.Length;
            RenderingStart = Text.Length / Width * Width;
        }

        private void MoveLeft()
        {
            if (CursorPosition > 0)
            {
                CursorPosition--;

                if (CursorPosition < RenderingStart)
                    RenderingStart -= Width;
            }
        }

        private void MoveRight()
        {
            if (CursorPosition < Text.Length)
            {
                CursorPosition++;

                if (CursorPosition >= Width + RenderingStart)
                    RenderingStart += Width;
            }
        }

        private void WriteCharacter(char character)
        {
            Text = Text.Insert(CursorPosition, character.ToString());
            MoveRight();
        }

        private void DeleteCharacter()
        {
            if (Text.Length > 0 && CursorPosition > 0)
            {
                Text = Text.Remove(CursorPosition - 1, 1);
                MoveLeft();
            }
        }
    }
}
