﻿using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using FileManager.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileManager.Components
{
    class FileEditor : ComponentGroup
    {
        private FileInfo File { get; set; }
        private MultilineTextBox TextBox { get; set; }
        private Label InfoBar { get; set; }
        public List<string> Lines => TextBox.Rows;

        public FileEditor(Dimension dimension, FileInfo file) : base(dimension)
        {
            File = file;           
            TextBox = new MultilineTextBox(new Dimension(Dimension.Absolute(0), Dimension.Absolute(0), Dimension.Full, Dimension.Offset(1)), FileService.GetRowsFromFile(File).ToList());
            Add(TextBox);
            InfoBar = new Label(new Dimension(Dimension.Absolute(0), Dimension.Offset(1), Dimension.Full, Dimension.Absolute(1)), $"{File.Name}     Ln 1, Col 1", bc: ApplicationColor.LighterBackground, fc: ApplicationColor.Foreground, fillToWidth: true);
            Add(InfoBar);

            TextBox.OnCursorMove += (position) => InfoBar.Text = $"{File.Name}     Ln {position.Y + 1}, Col {position.X + 1}";
        }
    }
}
