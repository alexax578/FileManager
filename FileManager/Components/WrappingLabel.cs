﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Utilities;

namespace FileManager.Components
{
    class WrappingLabel : Component
    {
        private string Text { get; set; }
        private TextAlign TextAlign { get; set; }

        public WrappingLabel(Dimension dimension, string text, TextAlign textAlign = TextAlign.Left) : base(dimension)
        {
            Text = text;
            TextAlign = textAlign;
        }

        public override void Render(Renderer renderer, bool focused)
        {
            List<string> wrappedText = WrapText(Text, Width);

            for (int i = 0; i < Height && i < wrappedText.Count; i++)
                renderer.AddText(RenderPosition.X, RenderPosition.Y + i, Width, wrappedText[i], fc: ApplicationColor.Foreground, textAlign: TextAlign);
        }

        private static List<string> WrapText(string text, int limit)
        {
            List<string> lines = new List<string>();
            List<string> words = text.Split(' ').ToList();           

            for (int i = 0; i < words.Count; i++)
            {
                if (words[i].Length > limit)
                {
                    IEnumerable<string> split = words[i].SplitIntoSegments(limit);
                    words.RemoveAt(i);
                    words.InsertRange(i, split);
                }
            }

            string line = "";
            foreach (string word in words)
            {
                if ((line + word).Length > limit)
                {
                    lines.Add(line);
                    line = "";
                }

                line += $"{word} ";
            }

            if (line.Length > 0)
                lines.Add(line);

            return lines;
        }
    }
}
