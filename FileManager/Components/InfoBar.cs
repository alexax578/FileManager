﻿using FileManager.Data_Structures;
using System;
using System.Collections.Generic;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;

namespace FileManager.Components
{
    class InfoBar : ComponentGroup
    {
        private List<string> Keys { get; }

        public InfoBar(Dimension dimension, params KeyLabelPair[] pairs) : base(dimension)
        {
            Keys = new List<string>();
            int pos = 0;

            for (int i = 0; i < pairs.Length; i++)
            {
                int j = i;
                int curPos = pos;
                KeyLabelPair pair = pairs[i];
                Keys.Add(pair.Key.ToString());
                Add(new Label(new Dimension(Dimension.Absolute(Keys[j].Length + curPos), Dimension.Offset(1), Dimension.Absolute(pair.Value.Length), Dimension.Absolute(1)), 
                    pair.Value, ApplicationColor.InfoBar, ApplicationColor.Background));

                pos += pair.Value.Length + Keys[i].Length + 1;
            }
        }

        public override void Render(Renderer renderer, bool focused)
        {
            renderer.AddLine(RenderPosition.X, RenderPosition.Y, Width, bc: ApplicationColor.Background);

            for (int i = 0; i < Components.Count; i++)
            {
                renderer.AddText(Components[i].RenderPosition.X - Keys[i].Length, Components[i].RenderPosition.Y, Keys[i].Length, Keys[i], fc: ApplicationColor.Foreground);
                Components[i].Render(renderer, focused);
            }
        }
    }
}
