﻿using System;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;
using Color = System.Drawing.Color;

namespace FileManager.Components
{
    class ActionTextBox : TextBox
    {
        private Func<ResultingAction> OnEnter { get; set; }

        public ActionTextBox(Dimension dimension, string value = "", Color? bc = null, Color? fc = null, TextAlign textAlign = TextAlign.Left, 
            bool fillToWidth = true, Func<ResultingAction> onEnter = null) : base(dimension, value, bc, fc, textAlign, fillToWidth) =>
            OnEnter = onEnter;

        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            ResultingAction result = base.HandleKey(keyInfo);

            if (result.Type.HasFlag(ResultingActionType.Unhandled))
                if (keyInfo.Key == ConsoleKey.Enter && OnEnter != null)
                    return OnEnter.Invoke();

            return result;
        }
    }
}
