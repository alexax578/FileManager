﻿using FileManager.Data_Structures;
using FileManager.Dialogs;
using FileManager.Enums;
using FileManager.Screens;
using FileManager.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Components
{
    //TODO: Bookmarks
    class FileExplorer : ComponentGroup
    {
        private List<FileSystemInfo> Files { get; set; }
        private ActionTextBox CurrentPathBox { get; set; }
        private ScrollTable ScrollTable { get; set; }       
        public DirectoryInfo Folder { get; set; }
        private Action RefreshAll { get; set; }

        public static List<FileSystemInfo> Clipboard = new List<FileSystemInfo>();

        public FileExplorer(Dimension dimension, string path, Action refreshAll) : base(dimension)
        {
            ScrollTable = new ScrollTable(new Dimension(Dimension.Absolute(0), Dimension.Absolute(0), Dimension.Full, Dimension.Full), "Name", "Size", "Modify time");
            Add(ScrollTable);
            CurrentPathBox = new ActionTextBox(new Dimension(Dimension.Absolute(1), Dimension.Absolute(0), Dimension.Offset(2), Dimension.Absolute(1)), 
                path, bc: null, fc: ApplicationColor.Foreground, textAlign: TextAlign.Center, fillToWidth: false, onEnter: ChangePath);
            CurrentPathBox.OnSelection += () => CurrentPathBox.CursorPosition = CurrentPathBox.Text.Length;
            Add(CurrentPathBox);
            ChangeFolder(new DirectoryInfo(path));
            RefreshAll = refreshAll;

            CurrentPathBox.OnSelection += CurrentPathBox.RecalculatePositions;
            CurrentPathBox.OnDeselection += () => CurrentPathBox.RenderingStart = 0;
            CurrentPathBox.OnDeselection += RefreshPath;
        }

        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            ResultingAction action = base.HandleKey(keyInfo);

            if (action.Type.HasFlag(ResultingActionType.Unhandled))
            {
                switch (keyInfo.Key)
                {
                    case ConsoleKey.Enter:
                        if (ScrollTable.SelectedRowIndex == 0)
                        {
                            if (Folder.Parent != null)
                                return ChangeFolder(Folder.Parent);

                            return new ResultingAction(new DriveSelectionDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 5, Dimension.Absolute(60), Dimension.Absolute(10)), (directory) => ChangeFolder(directory)));
                        }
                        else if (Files[ScrollTable.SelectedRowIndex - 1] is DirectoryInfo directory)
                            return ChangeFolder(directory);
                        else if (Files[ScrollTable.SelectedRowIndex - 1] is FileInfo file)
                        {
                            try
                            {
                                return new ResultingAction(new FileEditingScreen(file));
                            }
                            catch (Exception e)
                            {
                                return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), e));
                            }
                        }
                        break;

                    case ConsoleKey.D2:
                        return new ResultingAction(new FileCreationDialog(new Dimension(size => size / 2 - 40, size => size / 2 - 4, Dimension.Absolute(80), Dimension.Absolute(8)), Folder.FullName, RefreshAll, FileType.Directory));

                    case ConsoleKey.D3:
                        if (ScrollTable.SelectedRowIndex > 0)
                            return new ResultingAction(new DeletionDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), Files[ScrollTable.SelectedRowIndex - 1], RefreshAll));
                        break;

                    case ConsoleKey.D4:
                        if (ScrollTable.SelectedRowIndex > 0)
                            return new ResultingAction(new FileMovingDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), Files[ScrollTable.SelectedRowIndex - 1], RefreshAll, FileOperation.Rename));
                        break;

                    case ConsoleKey.D5:
                        if (ScrollTable.SelectedRowIndex > 0)
                            return new ResultingAction(new FileMovingDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), Files[ScrollTable.SelectedRowIndex - 1], RefreshAll, FileOperation.Move));
                        break;

                    case ConsoleKey.D6:
                        if (ScrollTable.SelectedRowIndex > 0)
                        {
                            Clipboard.Clear();
                            Clipboard.Add(Files[ScrollTable.SelectedRowIndex - 1]);
                        }
                        break;

                    case ConsoleKey.D7:
                        if (Clipboard.Count > 0)
                            return new ResultingAction(new PasteDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 6, Dimension.Absolute(60), Dimension.Absolute(12)), Clipboard, Folder.FullName, RefreshAll));
                        break;

                    case ConsoleKey.D8:
                        return new ResultingAction(new FileCreationDialog(new Dimension(size => size / 2 - 40, size => size / 2 - 4, Dimension.Absolute(80), Dimension.Absolute(8)), Folder.FullName, RefreshAll, FileType.File));

                    case ConsoleKey.F5:
                        Refresh();
                        return ResultingAction.Handled;
                }
            }

            return action;
        }

        private ResultingAction ChangeFolder(DirectoryInfo directory)
        {
            try
            {
                Files = directory.GetFileSystemInfos().ToList();
            }
            catch (UnauthorizedAccessException e)
            {
                return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), e));
            }

            Folder = directory;
            RefreshRows();         
            
            return ResultingAction.Handled;
        }

        public void Refresh()
        {
            Files = Folder.GetFileSystemInfos().ToList();
            RefreshRows();
        }

        private void RefreshRows()
        {
            Files = Files.OrderBy(x => x.GetType() == typeof(FileInfo)).ToList();
            List<ScrollTableRow> rows = new List<ScrollTableRow>() { new ScrollTableRow(FileTypeColor.Directory, "..", "", "")};
            rows.AddRange(RowService.GetFileSystemInfoRows(Files));
            ScrollTable.ChangeRows(rows);
            CurrentPathBox.Text = Folder.FullName;
        }

        private ResultingAction ChangePath()
        {
            if (Directory.Exists(CurrentPathBox.Text))
            {
                ResultingAction action = ChangeFolder(new DirectoryInfo(CurrentPathBox.Text));

                if (action.Type.HasFlag(ResultingActionType.Handled))
                    SelectedComponent = ScrollTable;

                return action;
            }

            return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), $"Directory \"{CurrentPathBox.Text}\" doesn't exist."));
        }

        private void RefreshPath() => CurrentPathBox.Text = Folder.FullName;
    }
}
