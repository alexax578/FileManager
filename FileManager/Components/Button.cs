﻿using System;
using ConsoleForms.Components;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Components
{
    class Button : SelectableComponent
    {
        private string Text { get; set; }
        private Func<ResultingAction> OnEnter { get; set; }

        public Button(Dimension dimension, string text, Func<ResultingAction> onEnter) : base(dimension)
        {
            Text = text;
            OnEnter = onEnter;
        }
        
        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            if (keyInfo.Key == ConsoleKey.Enter)
                return OnEnter.Invoke();

            return ResultingAction.Unhandled;
        }

        public override void Render(Renderer renderer, bool focused)
        {
            if (Selected && focused)
                renderer.AddText(RenderPosition.X, RenderPosition.Y, Width, Text, ApplicationColor.SelectedBackground, ApplicationColor.SelectedForeground, textAlign: TextAlign.Center);
            else
                renderer.AddText(RenderPosition.X, RenderPosition.Y, Width, Text, fc: ApplicationColor.Foreground, textAlign: TextAlign.Center);
        }
    }
}
