﻿using System;
using System.Collections.Generic;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Components
{
    class ScrollList : ScrollComponent<string>
    {
        public ScrollList(Dimension dimension) : base(dimension) =>
            Rows = new List<string>();
    
        public override void Render(Renderer renderer, bool focused)
        {
            for (int i = 0; i < Rows.Count - RenderStartingRow && i < Height; i++)
            {
                if (i + RenderStartingRow == SelectedRowIndex && Selected && focused)
                    renderer.AddText(RenderPosition.X, RenderPosition.Y + i, Width, Rows[i + RenderStartingRow], ApplicationColor.SelectedBackground, ApplicationColor.SelectedForeground, fillToWidth: true);
                else
                    renderer.AddText(RenderPosition.X, RenderPosition.Y + i, Width, Rows[i + RenderStartingRow], fc: ApplicationColor.Foreground);
            }
        }

        public override void Resize(Point origin, int width, int height)
        {
            base.Resize(origin, width, height);

            //Move RenderStartingRow so that selected row is in the rendering range.
            if (RenderStartingRow + Height < SelectedRowIndex)
                RenderStartingRow += SelectedRowIndex - RenderStartingRow - Height + 1;
        }
    }
}
