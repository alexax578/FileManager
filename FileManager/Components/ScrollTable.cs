﻿using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Utilities;
using FileManager.Data_Structures;
using FileManager.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FileManager.Components
{
    class ScrollTable : ScrollComponent<ScrollTableRow>
    {
        private int[] ColumnSizes { get; set; }
        private string[] Header { get; set; }
        private int RenderingWidth => Width - 2;

        public override Point ScrollRenderPosition => new Point(RenderPosition.X + 1, RenderPosition.Y + 2);
        public override int ScrollWidth => Width - 2;
        public override int ScrollHeight => Height - 3;

        public ScrollTable(Dimension dimension, params string[] header) : base(dimension) =>
            Header = header;

        public override void Render(Renderer renderer, bool focused)
        {     
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, bc: ApplicationColor.Component);           
            renderer.AddText(RenderPosition.X + 1, RenderPosition.Y + 1, ScrollWidth, GetHeaderLine(), fc: ApplicationColor.ScrollTableHeader);

            for (int i = 0; i < Rows.Count - RenderStartingRow && i < ScrollHeight; i++)
            {
                if (i + RenderStartingRow == SelectedRowIndex && Selected && focused)
                    renderer.AddText(ScrollRenderPosition.X, ScrollRenderPosition.Y + i, RenderingWidth, GetRowLine(i + RenderStartingRow), ApplicationColor.SelectedBackground, ApplicationColor.SelectedForeground, fillToWidth: true);
                else
                    renderer.AddText(ScrollRenderPosition.X, ScrollRenderPosition.Y + i, RenderingWidth, GetRowLine(i + RenderStartingRow), fc: Rows[i + RenderStartingRow].TextColor);
            }

            renderer.AddBoxWithSeparators(RenderPosition.X, RenderPosition.Y, Width, Height, ColumnSizes.Select(i => i + 1).DropLast().ToArray(), fc: ApplicationColor.Foreground);
        }

        public override void ChangeRows(List<ScrollTableRow> rows)
        {
            base.ChangeRows(rows);
            CalculateColumnSizes();
        }

        private string GetHeaderLine()
        {
            string result = "";

            for (int i = 0; i < Header.Length; i++)
                result += Header[i].Substring(0, ColumnSizes[i] < Header[i].Length ? ColumnSizes[i] : Header[i].Length).Center(ColumnSizes[i] + 1);

            return result;
        }

        private string GetRowLine(int index)
        {
            string result = "";

            for (int i = 0; i < Rows[index].Values.Length; i++)
            {
                string value = Rows[index].Values[i];
                result += value.Substring(0, ColumnSizes[i] < value.Length ? ColumnSizes[i] : value.Length).PadRight(ColumnSizes[i] + 1);
            }

            return result;
        }

        private void CalculateColumnSizes()
        {
            if (Rows.Count > 0)
            {
                ColumnSizes = new int[Rows[0].Values.Length];
                int widthForColumns = RenderingWidth - ColumnSizes.Length + 1; //Get width that is left for column values to appear in.

                for (int i = 0; i < Rows[0].Values.Length; i++)
                    ColumnSizes[i] = Rows.Max(x => x.Values[i].Length);

                //Widen columns
                if (ColumnSizes.Sum() < widthForColumns)
                    for (int i = 0; ColumnSizes.Sum() < widthForColumns; i++)
                        ColumnSizes[i %= ColumnSizes.Length]++;

                //Shorten columns
                else if (ColumnSizes.Sum() > widthForColumns)
                    for (int i = 0; ColumnSizes.Sum() > widthForColumns; i++)
                        ColumnSizes[Array.IndexOf(ColumnSizes, ColumnSizes.Max())]--;
            }
        }

        public override void Resize(Point origin, int width, int height)
        {
            base.Resize(origin, width, height);
            CalculateColumnSizes();
        }
    }
}
