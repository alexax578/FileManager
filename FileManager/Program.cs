﻿using System;
using System.Runtime.InteropServices;
using ConsoleForms;
using Console = Colorful.Console;
using FileManager.Screens;

namespace FileManager
{
    sealed class Program
    {
        private const int STD_INPUT_HANDLE = -10;
        private const int STD_OUTPUT_HANDLE = -11;

        private const uint DISABLE_NEWLINE_AUTO_RETURN = 0x0008;
        private const uint ENABLE_QUICK_EDIT_MODE = 0x0040;

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll")]
        private static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

        [DllImport("kernel32.dll")]
        private static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);

        private static void Main()
        {
            #region Initial console setup
            IntPtr handle = GetConsoleWindow();
            IntPtr inputHandle = GetStdHandle(STD_INPUT_HANDLE);
            IntPtr outputHandle = GetStdHandle(STD_OUTPUT_HANDLE);

            if (handle != IntPtr.Zero)
            {
                //Disable selecting and editing of text with mouse
                if (GetConsoleMode(inputHandle, out uint inputMode))
                    SetConsoleMode(inputHandle, inputMode &= ~ENABLE_QUICK_EDIT_MODE);

                //Disable auto new line when end of the line is reached by writing into it.
                if (GetConsoleMode(outputHandle, out uint outputMode))
                    SetConsoleMode(outputHandle, outputMode &= DISABLE_NEWLINE_AUTO_RETURN);
            }

            Console.Title = "File Manager";
            Console.CursorVisible = false;
            #endregion

            Application application = new Application<ExplorerScreen>(160, 40);
            application.Run();
        }
    }
}
