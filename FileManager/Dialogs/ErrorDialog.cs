﻿using FileManager.Components;
using System;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class ErrorDialog : Dialog
    {
        public ErrorDialog(Dimension dimension, Exception exception) : base(dimension) =>
            OnInit(exception.Message);

        public ErrorDialog(Dimension dimension, string error) : base(dimension) =>
            OnInit(error);

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Error);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }

        private void OnInit(string error)
        {
            Components.Add(new WrappingLabel(new Dimension(Dimension.Eighth, Dimension.Quarter, size => size / 4 * 3, Dimension.Half), error, TextAlign.Center));
            Components.Add(new Button(new Dimension(size => size / 2 - 1, Dimension.Offset(2), Dimension.Absolute(2), Dimension.Absolute(1)), "Ok", () => ResultingAction.CloseDialog));
        }
    }
}
