﻿using FileManager.Components;
using System;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class HelpDialog : Dialog
    {
        public HelpDialog(Dimension dimension) : base(dimension)
        {
            Components.Add(new Label(new Dimension(Dimension.Absolute(0), Dimension.Absolute(1), Dimension.Full, Dimension.Absolute(1)), @"There is no help! >.<", fc: ApplicationColor.Foreground, textAlign: TextAlign.Center));
            Components.Add(new Button(new Dimension(size => size / 2 - 2, Dimension.Offset(2), Dimension.Absolute(5), Dimension.Absolute(1)), "Close", () => ResultingAction.CloseDialog));
        }

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }
    }
}
