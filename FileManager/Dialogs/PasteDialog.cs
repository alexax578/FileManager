﻿using FileManager.Components;
using FileManager.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class PasteDialog : Dialog
    {
        private List<FileSystemInfo> Infos { get; set; }
        private TextBox PastePathBox { get; set; }
        private Action OnPaste { get; set; }

        public PasteDialog(Dimension dimension, List<FileSystemInfo> infos, string pasteLocation, Action onPaste) : base(dimension)
        {
            Infos = infos;
            OnPaste = onPaste;

            string caption = $"Paste {(Infos.Count > 1 ? "files" : "a file")}";
            Components.Add(new Label(new Dimension(size => size / 2 - caption.Length / 2, Dimension.Absolute(1), Dimension.Absolute(caption.Length), Dimension.Absolute(1)), caption, fc: ApplicationColor.Foreground));

            ScrollList scrollList = new ScrollList(new Dimension(Dimension.Eighth, Dimension.Absolute(3), size => size / 4 * 3, Dimension.Offset(6)));
            scrollList.ChangeRows(RowService.GetFileSystemInfoNames(Infos).ToList());
            Components.Add(scrollList);

            string nameLabel = "path: ";

            Components.Add(new Label(new Dimension(Dimension.Tenth, Dimension.Offset(4), Dimension.Absolute(nameLabel.Length), Dimension.Absolute(1)), nameLabel, fc: ApplicationColor.Foreground));
            PastePathBox = new ActionTextBox(new Dimension(size => size / 10 + nameLabel.Length, Dimension.Offset(4), size => (int)(size / 1.25) - nameLabel.Length, 
                Dimension.Absolute(1)), pasteLocation, bc: ApplicationColor.Foreground, fc: ApplicationColor.Background, onEnter: DeleteFiles);
            Components.Add(PastePathBox);

            string paste = "Paste";
            string cancel = "Cancel";

            Components.Add(new Button(new Dimension(size => size / 2 - paste.Length - 1, Dimension.Offset(2), Dimension.Absolute(paste.Length), Dimension.Absolute(1)), paste, DeleteFiles));
            Components.Add(new Button(new Dimension(size => size / 2 + 1, Dimension.Offset(2), Dimension.Absolute(cancel.Length), Dimension.Absolute(1)), cancel, () => ResultingAction.CloseDialog));

            ResultingAction DeleteFiles()
            {
                if (Directory.Exists(PastePathBox.Text))
                {
                    foreach (FileSystemInfo info in Infos)
                    {
                        try
                        {
                            if (info is FileInfo file)
                                file.CopyTo(PastePathBox.Text);
                            else if (info is DirectoryInfo directory)
                                FileService.CopyDirectory(directory, PastePathBox.Text);
                        }
                        catch (Exception e)
                        {
                            return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 5, Dimension.Absolute(60), Dimension.Absolute(10)), e));
                        }
                    }

                    OnPaste.Invoke();
                    return ResultingAction.CloseDialog;
                }
                else
                    return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), $"Path \"{PastePathBox.Text}\" is invalid."));
            }
        }

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }
    }
}
