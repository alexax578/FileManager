﻿using FileManager.Components;
using System;
using System.IO;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class DeletionDialog : Dialog
    {
        private FileSystemInfo Info { get; set; }
        private Action OnDelete { get; set; }

        public DeletionDialog(Dimension dimension, FileSystemInfo info, Action onDelete) : base(dimension) =>
            OnInit(info, onDelete);

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }

        private void OnInit(FileSystemInfo info, Action onDelete)
        {
            Info = info;
            OnDelete = onDelete;

            string caption = $"Are you sure you want to delete {info.Name}?";
            Components.Add(new WrappingLabel(new Dimension(Dimension.Eighth, Dimension.Absolute(1), size => size / 4 * 3, Dimension.Offset(4)), caption, TextAlign.Center));

            string delete = "Delete";
            string cancel = "Cancel";

            //TODO: Recycle bin - differently
            Components.Add(new Button(new Dimension(size => size / 2 - delete.Length - 1, Dimension.Offset(2), Dimension.Absolute(delete.Length), Dimension.Absolute(1)), delete, () =>
            {
            try
            {
                if (Info is FileInfo)
                    Info.Delete();
                else if (Info is DirectoryInfo directory)
                    directory.Delete(true);
            }
            catch (Exception e)
            {
                return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30,  size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), e));
                }

                OnDelete.Invoke();
                return ResultingAction.CloseDialog;
            }));
            Components.Add(new Button(new Dimension(size => size / 2 + 1, Dimension.Offset(2), Dimension.Absolute(cancel.Length), Dimension.Absolute(1)), cancel, () => ResultingAction.CloseDialog));
        }
    }
}
