﻿using FileManager.Components;
using FileManager.Enums;
using System;
using System.IO;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class FileCreationDialog : Dialog
    {
        private string FilePath { get; set; }
        private TextBox FileNameBox { get; set; }
        private Action OnCreate { get; set; }
        private FileType Type { get; set; }

        public FileCreationDialog(Dimension dimension, string path, Action onCreate, FileType type) : base(dimension)
        {
            FilePath = path;
            OnCreate = onCreate;
            Type = type;

            string caption = $"Create a {Type.ToString()}";

            Components.Add(new Label(new Dimension(size => size / 2 - caption.Length / 2, Dimension.Absolute(1), Dimension.Absolute(caption.Length), Dimension.Absolute(1)), caption, fc: ApplicationColor.Foreground));

            string nameLabel = "name: ";

            Components.Add(new Label(new Dimension(Dimension.Tenth, Dimension.Absolute(3), Dimension.Absolute(nameLabel.Length), Dimension.Absolute(1)), nameLabel, fc: ApplicationColor.Foreground));
            FileNameBox = new ActionTextBox(new Dimension(size => size / 10 + nameLabel.Length, Dimension.Absolute(3), size => (int)(size / 1.25) - nameLabel.Length, Dimension.Absolute(1)), bc: ApplicationColor.Foreground, fc: ApplicationColor.Background, onEnter: createFile);
            Components.Add(FileNameBox);

            string create = "Create";
            string cancel = "Cancel";

            Components.Add(new Button(new Dimension(size => size / 2 - create.Length - 1, Dimension.Offset(2), Dimension.Absolute(create.Length), Dimension.Absolute(1)), create, createFile));
            Components.Add(new Button(new Dimension(size => size / 2 + 1, Dimension.Offset(2), Dimension.Absolute(cancel.Length), Dimension.Absolute(1)), cancel, () => ResultingAction.CloseDialog));

            ResultingAction createFile()
            {
                try
                {
                    if (Type == FileType.Directory)
                        Directory.CreateDirectory(Path.Combine(FilePath, FileNameBox.Text));
                    else if (Type == FileType.File)
                        File.Create(Path.Combine(FilePath, FileNameBox.Text)).Dispose();
                }
                catch (NotSupportedException)
                {
                    return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), $"Invalid file name \"{FileNameBox.Text}\"."));
                }
                catch (Exception e)
                {
                    return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 5, Dimension.Absolute(60), Dimension.Absolute(10)), e));
                }
                OnCreate.Invoke();
                return ResultingAction.CloseDialog;
            }
        }

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }
    }
}
