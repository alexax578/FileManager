﻿using FileManager.Components;
using FileManager.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class DriveSelectionDialog : Dialog
    {
        private List<DriveInfo> Drives { get; set; }
        private Action<DirectoryInfo> ChangeDrive { get; set; }
        private ScrollList ScrollList { get; set; }

        public DriveSelectionDialog(Dimension dimension, Action<DirectoryInfo> changeDrive) : base(dimension)
        {
            ChangeDrive = changeDrive;
            Drives = DriveInfo.GetDrives().ToList();

            Components.Add(new Label(new Dimension(Dimension.Absolute(1), Dimension.Absolute(1), Dimension.Offset(2), Dimension.Absolute(1)), "Select a drive", fc: ApplicationColor.Foreground));
            ScrollList = new ScrollList(new Dimension(Dimension.Absolute(1), Dimension.Absolute(3), Dimension.Offset(2), Dimension.Offset(5)));
            Components.Add(ScrollList);
            ScrollList.ChangeRows(RowService.GetDriveNames(Drives).ToList());
            Components.Add(new Button(new Dimension(size => size / 2 - 3, Dimension.Offset(2), Dimension.Absolute(6), Dimension.Absolute(1)), "Cancel", () => ResultingAction.CloseDialog));
        }
        
        public override ResultingAction HandleKey(ConsoleKeyInfo keyInfo)
        {
            if (keyInfo.Key == ConsoleKey.Enter && ScrollList.Selected)
            {
                try
                {
                    ChangeDrive.Invoke(Drives[ScrollList.SelectedRowIndex].RootDirectory);
                }
                catch (IOException)
                {
                    return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)),"Zařízení není připraveno."));
                }
                return ResultingAction.CloseDialog;
            }

            return base.HandleKey(keyInfo);
        }

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }
    }
}
