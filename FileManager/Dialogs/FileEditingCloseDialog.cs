﻿using System;
using System.Collections.Generic;
using System.IO;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;
using FileManager.Components;
using FileManager.Utilities;

namespace FileManager.Dialogs
{
    class FileEditingCloseDialog : Dialog
    {
        public FileEditingCloseDialog(Dimension dimension, FileEditor editor, FileInfo file, params KeyValuePair<ConsoleKey, Func<ResultingAction>>[] pairs) : base(dimension, pairs)
        {
            Components.Add(new WrappingLabel(new Dimension(Dimension.Absolute(2), Dimension.Absolute(2), Dimension.Offset(4), Dimension.Offset(3)), $"Do you want to save changes to {file.Name}?", TextAlign.Center));

            string save = "save";
            string dont = "don't save";
            string cancel = "cancel";
            int HalfSizeDiff = (save.Length - cancel.Length) / 2;

            Components.Add(new Button(new Dimension(size => size / 2 - dont.Length / 2 - save.Length - 4 + HalfSizeDiff, Dimension.Offset(2), Dimension.Absolute(save.Length), Dimension.Absolute(1)), save, 
                () => { FileService.RewriteFile(file, editor.Lines); return ResultingAction.CloseDialogAndScreen; }));
            Components.Add(new Button(new Dimension(size => size / 2 - dont.Length / 2 + HalfSizeDiff, Dimension.Offset(2), Dimension.Absolute(dont.Length), Dimension.Absolute(1)), dont, () => ResultingAction.CloseDialogAndScreen));
            Components.Add(new Button(new Dimension(size => size / 2 + dont.Length / 2 + 4 + HalfSizeDiff, Dimension.Offset(2), Dimension.Absolute(cancel.Length), Dimension.Absolute(1)), cancel, () => ResultingAction.CloseDialog));
        }

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }
    }
}
