﻿using FileManager.Components;
using FileManager.Enums;
using System;
using System.IO;
using ConsoleForms.Data_Structures;
using ConsoleForms.Rendering;
using ConsoleForms.Windows;

namespace FileManager.Dialogs
{
    class FileMovingDialog : Dialog
    {
        private FileSystemInfo Info { get; set; }
        private TextBox PathBox { get; set; }
        private Action OnMove { get; set; }
        private FileOperation Operation { get; set; }

        public FileMovingDialog(Dimension dimension, FileSystemInfo info, Action onMove, FileOperation operation) : base(dimension)
        {
            Info = info;
            OnMove = onMove;
            Operation = operation;

            string action = operation.ToString();

            string caption = $"{action} {(Info is DirectoryInfo ? "directory" : "file")}";
            Components.Add(new Label(new Dimension(size => size / 2 - caption.Length / 2, Dimension.Absolute(1), Dimension.Absolute(caption.Length), Dimension.Absolute(1)), caption, fc: ApplicationColor.Foreground));

            string nameLabel = "";
            string textboxValue = "";

            if (Operation == FileOperation.Move)
            {
                nameLabel = "path: ";
                textboxValue = Info.FullName;
            }
            else if (Operation == FileOperation.Rename)
            {
                nameLabel = "name: ";
                textboxValue = Info.Name;
            }

            Components.Add(new Label(new Dimension(Dimension.Tenth, Dimension.Absolute(3), Dimension.Absolute(nameLabel.Length), Dimension.Absolute(1)), nameLabel, fc: ApplicationColor.Foreground));
            PathBox = new ActionTextBox(new Dimension(size => size / 10 + nameLabel.Length, Dimension.Absolute(3), size => (int)(size / 1.25) - nameLabel.Length, 
                Dimension.Absolute(1)), textboxValue, bc: ApplicationColor.Foreground, fc: ApplicationColor.Background, onEnter: renameFile);
            Components.Add(PathBox);

            string cancel = "Cancel";

            Components.Add(new Button(new Dimension(size => size / 2 - action.Length - 1, Dimension.Offset(2), Dimension.Absolute(action.Length), Dimension.Absolute(1)), action, renameFile));
            Components.Add(new Button(new Dimension(size => size / 2 + 1, Dimension.Offset(2), Dimension.Absolute(cancel.Length), Dimension.Absolute(1)), cancel, () => ResultingAction.CloseDialog));

            ResultingAction renameFile()
            {
                try
                {
                    if (Info is DirectoryInfo dir)
                    {
                        if (Operation == FileOperation.Rename)
                            dir.MoveTo(Path.Combine(dir.Parent.FullName, PathBox.Text));
                        else if (Operation == FileOperation.Move)
                            dir.MoveTo(Path.Combine(PathBox.Text, dir.Name));
                    }
                    else if (Info is FileInfo file)
                    {
                        if (Operation == FileOperation.Rename)
                            file.MoveTo(Path.Combine(file.DirectoryName, PathBox.Text));
                        else if (Operation == FileOperation.Move)
                            file.MoveTo(Path.Combine(PathBox.Text, file.Name));
                    }
                }
                catch (Exception e)
                {
                    return new ResultingAction(new ErrorDialog(new Dimension(size => size / 2 - 30, size => size / 2 - 4, Dimension.Absolute(60), Dimension.Absolute(8)), e));
                }

                OnMove.Invoke();
                return ResultingAction.CloseDialog;
            }
        }

        protected override void InnerRender(Renderer renderer)
        {
            renderer.FillBox(RenderPosition.X, RenderPosition.Y, Width, Height, character: ' ', bc: ApplicationColor.Dialog);
            renderer.AddBox(RenderPosition.X, RenderPosition.Y, Width, Height, fc: ApplicationColor.Foreground);
        }        
    }
}
